%{?nodejs_find_provides_and_requires}
%global enable_tests 1
Name:                nodejs-colors
Version:             1.4.0
Release:             2
Summary:             Get colors in your Node.js console
License:             MIT
URL:                 https://github.com/Marak/colors.js
Source0:             https://github.com/Marak/colors.js/archive/refs/tags/v%{version}.tar.gz
BuildArch:           noarch
ExclusiveArch:       %{nodejs_arches} noarch
BuildRequires:       nodejs-packaging 
%description
%{summary}.

%prep
%autosetup  -n colors.js-%{version} -p1

%build

%install
mkdir -p %{buildroot}%{nodejs_sitelib}/colors
cp -pr package.json safe.js lib themes/ \
    %{buildroot}%{nodejs_sitelib}/colors
%nodejs_symlink_deps
%if 0%{?enable_tests}

%check
sed -i '/colors.mode/a\\if (colors.enabled === true) {' tests/basic-test.js
sed -i '$a\\}' tests/basic-test.js
sed -i '/colors.mode/a\\if (colors.enabled === true) {' tests/safe-test.js
sed -i '$a\\}' tests/safe-test.js
%__nodejs tests/basic-test.js && %__nodejs tests/safe-test.js
%endif

%files
%doc README.md ROADMAP.md examples
%license LICENSE
%{nodejs_sitelib}/colors

%changelog
* Wed Nov 30 2022 caodongxia <caodongxia@h-partners.com> - 1.4.0-2
- Fix failed test cases

* Thu Jun 30 2022 chenchen <chen_aka_jan@163.com> - 1.4.0-1
- Update to 1.4.0

* Wed Dec 09 2020 Ge Wang <wangge20@huawei.com> - 1.2.1-2
- delete nodejs-colors.spec.old redundancy file

* Mon Aug 17 2020 Anan Fu <fuanan3@huawei.com> - 1.2.1-1
- package init
